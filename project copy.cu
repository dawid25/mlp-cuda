#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <random>

// Definicja struktury przechowującej dane jednego rekordu IrisDataset
struct IrisRecord {
    float sepalLength;
    float sepalWidth;
    float petalLength;
    float petalWidth;
    int species;
};

// Funkcja zamieniająca nazwy gatunków roślin na liczby całkowite
int mapSpeciesToInt(const std::string& species) {
    static std::unordered_map<std::string, int> speciesMap = {
        {"Iris-setosa", 0},
        {"Iris-versicolor", 1},
        {"Iris-virginica", 2}
    };

    auto it = speciesMap.find(species);
    if (it != speciesMap.end()) {
        return it->second;
    }
    return -1; // Nieznany gatunek
}

// Funkcja do wczytywania danych z pliku CSV po stronie hosta za pomocą std::getline
void readIrisDataset(const char* filename, std::vector<IrisRecord>& records, std::vector<int>& species) {
    // Wczytywanie danych z pliku CSV za pomocą std::getline
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Error: Unable to open file " << filename << std::endl;
        return;
    }

    std::string line;

    while (std::getline(file, line)) {
        std::stringstream ss(line);
        IrisRecord record;

        // Odczytaj wartości zmiennoprzecinkowe
        ss >> record.sepalLength;
        ss.ignore(); // Ignoruj przecinek
        ss >> record.sepalWidth;
        ss.ignore();
        ss >> record.petalLength;
        ss.ignore();
        ss >> record.petalWidth;

        // Odczytaj gatunek
        std::string speciesStr;
        ss.ignore();
        ss >> speciesStr;

        // Zamień nazwę gatunku na liczbę całkowitą
        record.species = mapSpeciesToInt(speciesStr);

        // Dodaj rekord do wektorów
        records.push_back(record);
        species.push_back(record.species);
    }
}

// Funkcja do tasowania danych
void shuffleData(std::vector<IrisRecord>& records, std::vector<int>& species) {
    // Seed generatora liczb losowych
    std::random_device rd;
    // Tworzenie generatora liczb losowych
    std::mt19937 g(rd());

    // Tasowanie danych
    std::shuffle(records.begin(), records.end(), g);
    std::shuffle(species.begin(), species.end(), g);
}

int main() {
    const char* filename = "iris.csv";

    // Host data
    std::vector<IrisRecord> records;
    std::vector<int> species;

    // Wczytaj dane z pliku CSV
    readIrisDataset(filename, records, species);

    // Tasuj dane
    shuffleData(records, species);

    // Podziel dane na treningowe (80%) i walidacyjne (20%)
    size_t splitIndex = static_cast<size_t>(records.size() * 0.8);

    // Wektory dla zbiorów treningowego i walidacyjnego
    std::vector<IrisRecord> trainRecords(records.begin(), records.begin() + splitIndex);
    std::vector<int> trainSpecies(species.begin(), species.begin() + splitIndex);

    std::vector<IrisRecord> validationRecords(records.begin() + splitIndex, records.end());
    std::vector<int> validationSpecies(species.begin() + splitIndex, species.end());

    // Wyświetl dane zbioru treningowego
    std::cout << "Training Set:" << std::endl;
    for (size_t i = 0; i < trainRecords.size(); ++i) {
        std::cout << "Record " << i + 1 << ": "
                  << trainRecords[i].sepalLength << ", "
                  << trainRecords[i].sepalWidth << ", "
                  << trainRecords[i].petalLength << ", "
                  << trainRecords[i].petalWidth << ", "
                  << "Species: " << trainSpecies[i] << std::endl;
    }

    // Wyświetl dane zbioru walidacyjnego
    std::cout << "\nValidation Set:" << std::endl;
    for (size_t i = 0; i < validationRecords.size(); ++i) {
        std::cout << "Record " << i + 1 << ": "
                  << validationRecords[i].sepalLength << ", "
                  << validationRecords[i].sepalWidth << ", "
                  << validationRecords[i].petalLength << ", "
                  << validationRecords[i].petalWidth << ", "
                  << "Species: " << validationSpecies[i] << std::endl;
    }

    return 0;
}
