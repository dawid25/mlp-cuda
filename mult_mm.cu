#include <stdio.h>
#include "matUtils.h"

#define RND_SEED 13 // for tests reproducibility

// Compute C = A * B general matrix-matrix multiply
__global__ void standardMatrixMult(float *A, float *B, float *C, int numARows,
                                   int numAColumns, int numBRows, int numBColumns)
{
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    if (row < numARows && col < numBColumns)
    {
        float sum = 0;
        for (int ii = 0; ii < numAColumns; ii++)
        {
            sum += A[row * numAColumns + ii] * B[ii * numBColumns + col];
        }
        C[row * numBColumns + col] = sum;
    }
}

#define TILE_WIDTH 16

//@@ INSERT CODE HERE
// Compute C = A * B tiled matrix-matrix multiply
__global__ void matrixMultiply(float *A, float *B, float *C, int numARows, int numAColumns,
                               int numBRows, int numBColumns)
{
    // Define shared memory arrays for matrices A and B
    __shared__ float ds_A[TILE_WIDTH][TILE_WIDTH];
    __shared__ float ds_B[TILE_WIDTH][TILE_WIDTH];

    // Calculate the row and column indices for the current thread
    int row = blockIdx.y * TILE_WIDTH + threadIdx.y;
    int col = blockIdx.x * TILE_WIDTH + threadIdx.x;

    float result = 0.0f;

    // Iterate over the phases (blocks of size TILE_WIDTH)
    for (int phase = 0; phase < (numAColumns - 1) / TILE_WIDTH + 1; ++phase)
    {
        // Calculate indices for elements of matrix A
        int aRow = row;
        int aCol = phase * TILE_WIDTH + threadIdx.x;
        
        // Calculate indices for elements of matrix B
        int bRow = phase * TILE_WIDTH + threadIdx.y;
        int bCol = col;

        // Load elements of matrix A into shared memory
        if (aRow < numARows && aCol < numAColumns)
        {
            ds_A[threadIdx.y][threadIdx.x] = A[aRow * numAColumns + aCol];
        }
        else
        {
            ds_A[threadIdx.y][threadIdx.x] = 0.0;
        }

        // Load elements of matrix B into shared memory
        if (bRow < numBRows && bCol < numBColumns)
        {
            ds_B[threadIdx.y][threadIdx.x] = B[bRow * numBColumns + bCol];
        }
        else
        {
            ds_B[threadIdx.y][threadIdx.x] = 0.0;
        }

        // Synchronize threads to ensure all shared memory is loaded
        __syncthreads();

        // Calculate the dot product for this phase
        for (int i = 0; i < TILE_WIDTH; ++i)
        {
            result += ds_A[threadIdx.y][i] * ds_B[i][threadIdx.x];
        }

        // Synchronize threads before proceeding to the next phase
        __syncthreads();
    }

    // Store the result in matrix C
    if (row < numARows && col < numBColumns)
    {
        C[row * numBColumns + col] = result;
    }
}

void generateRandomFlattenMatrix(float *M, unsigned int size)
{
    for (int i = 0; i < size; ++i)
    {
        M[i] = (rand() % 20) + 50;
    }
}

int main(int argc, char **argv)
{
    // check if number of input args is correct
    if (argc < 4 || argc > 7)
    {
        printf("Wrong number of arguments: 3 mandatory arguments needed (width A, height A and width B)\n");
        printf("If the 4th argument is --read then input matrices are read from the files given as 5th and 6th arguments.\n");
        printf("Example: ./mult_mm.out 5 8 13 --read ./inputA.txt ./inputB.txt\n");
        return 0;
    }
    int widthA = atoi(argv[1]);
    int heightA = atoi(argv[2]);
    int widthB = atoi(argv[3]);

    int readFile = 0;
    int matAsize = widthA * heightA;
    int matBsize = widthB * widthA;
    int matCsize = heightA * widthB;

    if (argc > 4)
    {
        // Check matrix A size
        int status = getMatSize(argv[5], &matAsize);
        if (status != MAT_SUCCESS)
        {
            printf("Error: getMatSize for matrix A, status: %d\n", status);
            return 0;
        }
        if (matAsize != widthA * heightA)
        {
            printf("Matrix A size mismatch: %d vs %d.\n", matAsize, widthA * heightA);
            return 0;
        }

        // Check matrix B size
        status = getMatSize(argv[6], &matBsize);
        if (status != MAT_SUCCESS)
        {
            printf("Error: getMatSize for matrix B, status: %d\n", status);
            return 0;
        }
        if (matBsize != widthB * widthA)
        {
            printf("Matrix B size mismatch: %d vs %d.\n", matBsize, widthB * widthA);
            return 0;
        }

        readFile = 1;
    }

    float *h_A = (float *)malloc(matAsize * sizeof(float));
    float *h_B = (float *)malloc(matBsize * sizeof(float));
    float *h_C = (float *)malloc(matCsize * sizeof(float));
    // Allocate memory for the result from standardMatrixMult kernel
    float *h_C_standard = (float *)malloc(matCsize * sizeof(float));
    float *d_A, *d_B, *d_C;
    cudaMalloc((void **)&d_A, matAsize * sizeof(float));
    cudaMalloc((void **)&d_B, matBsize * sizeof(float));
    cudaMalloc((void **)&d_C, matCsize * sizeof(float));

    if (!readFile)
    {
        srand(RND_SEED);

        // Generate matrix A
        int status = generateMat("./inputA.txt", h_A, matAsize);
        if (status != MAT_SUCCESS)
        {
            printf("Error: generateMat for matrix A, status: %d\n", status);
            return 0;
        }

        // Generate matrix B
        status = generateMat("./inputB.txt", h_B, matBsize);
        if (status != MAT_SUCCESS)
        {
            printf("Error: generateMat for matrix B, status: %d\n", status);
            return 0;
        }
    }
    else
    {
        // Read matrix A
        int status = readMat(argv[5], h_A, matAsize);
        if (status != MAT_SUCCESS)
        {
            printf("Error: readMat for matrix A, status: %d\n", status);
            return 0;
        }

        // Read matrix B
        status = readMat(argv[6], h_B, matBsize);
        if (status != MAT_SUCCESS)
        {
            printf("Error: readMat for matrix B, status: %d\n", status);
            return 0;
        }
    }

    // Copy data from host to device
    cudaMemcpy(d_A, h_A, matAsize * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, h_B, matBsize * sizeof(float), cudaMemcpyHostToDevice);

    // Define grid and block dimensions
    dim3 dimGrid((widthB - 1) / TILE_WIDTH + 1, (heightA - 1) / TILE_WIDTH + 1);
    dim3 dimBlock(TILE_WIDTH, TILE_WIDTH);

    // Call the matrixMultiply kernel
    matrixMultiply<<<dimGrid, dimBlock>>>(d_A, d_B, d_C, heightA, widthA, widthA, widthB);

    // Call the standardMatrixMult kernel
    standardMatrixMult<<<dimGrid, dimBlock>>>(d_A, d_B, d_C, heightA, widthA, widthA, widthB);


    // Check for kernel launch errors
    cudaError_t cudaError = cudaGetLastError();
    if (cudaError != cudaSuccess)
    {
        fprintf(stderr, "CUDA kernel launch error: %s\n", cudaGetErrorString(cudaError));
        return 1;
    }

    // Check for kernel launch errors for standardMatrixMult
    cudaError_t cudaErrorStandard = cudaGetLastError();
    if (cudaErrorStandard != cudaSuccess)
    {
        fprintf(stderr, "CUDA standardMatrixMult kernel launch error: %s\n", cudaGetErrorString(cudaErrorStandard));
        return 1;
    }

    // Copy the result from device to host
    cudaMemcpy(h_C, d_C, matCsize * sizeof(float), cudaMemcpyDeviceToHost);

    // Check if the results match
    bool resultsMatch = true;
    for (int i = 0; i < matCsize; ++i)
    {
        if (h_C[i] != h_C_standard[i])
        {
            resultsMatch = false;
            break;
        }
    }

    // Save the output values
    FILE *fp = fopen("mult_mm_C_out.txt", "w");
    if (fp == NULL)
    {
        fprintf(stderr, "Cannot open output file!\n");
    }
    else
    {
        printf("Generating output file... ");
        for (int i = 0; i < matCsize; ++i)
        {
            fprintf(fp, "%.0f ", h_C[i]);
        }
        printf("DONE!\n");
        fclose(fp);
    }

    // Output whether the results match
    if (resultsMatch)
    {
        printf("Results from matrixMultiply and standardMatrixMult match.\n");
    }
    else
    {
        printf("Results from matrixMultiply and standardMatrixMult do not match.\n");
    }

    // Free allocated memory
    free(h_C_standard);
    free(h_A);
    free(h_B);
    free(h_C);
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    return 0;
}
